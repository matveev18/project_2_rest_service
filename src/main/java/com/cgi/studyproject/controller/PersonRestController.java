package com.cgi.studyproject.controller;


import com.cgi.studyproject.persistence.entities.Person;
import com.cgi.studyproject.persistence.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/persons")
public class PersonRestController {


    private PersonService personService;

    @Autowired
    public PersonRestController(PersonService personService) {
        this.personService = personService;
    }




    /*   private static final String template = "Hello, %s!";
        private final AtomicLong counter = new AtomicLong();

        @GetMapping("/greeting")
        public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
            return new Greeting(counter.incrementAndGet(), String.format(template, name));
        }
    */
    //GET all users
    @GetMapping(value = "/all")
    @ResponseBody
    public List<Person> getAllUsers() {
        return personService.getAllPersons();
    }



}