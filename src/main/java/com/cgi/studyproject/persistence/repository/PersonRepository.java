package com.cgi.studyproject.persistence.repository;


import com.cgi.studyproject.persistence.entities.Person;
import com.cgi.studyproject.persistence.mapper.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Person getPersonById(Long id) {
        // return new Person(1L, "Pavel", "Seda");

        return jdbcTemplate.queryForObject("SELECT * FROM person WHERE person.id_person=?",
                new Object[]{id}, new PersonMapper());
    }

    public List<Person> getAllPersons() {
        return jdbcTemplate.query("SELECT * FROM person", new PersonMapper());
    }
/*
    public List<Person> getAllPersons() {
        return jdbcTemplate.query("SELECT * FROM person",
                (rs, rowNum) ->
                        new Person(
                                rs.getLong("id_person"),
                                rs.getString("name"),
                             //   rs.getString("email"),
                             //   rs.getString("password"),
                            //    rs.getInt("salary")

                        )
                        );

    }
*/

    public void addPerson(Person person) {
        jdbcTemplate.update("INSERT INTO person (id_person, name, email) VALUES (?, ?, ?)",
                person.getIdPerson(), person.getNamePerson(), person.getEmail());


    }


    public void updatePersonById(Long id, Person person) {
//jdbcTemplate.update()
    }






}
