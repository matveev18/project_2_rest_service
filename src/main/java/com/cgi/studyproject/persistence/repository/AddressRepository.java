package com.cgi.studyproject.persistence.repository;


import com.cgi.studyproject.persistence.entities.Address;
import com.cgi.studyproject.persistence.mapper.AddressMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AddressRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public AddressRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Address getAddressById(Long id){
        return jdbcTemplate.queryForObject("SELECT * FROM address a WHERE a.id_address=?",
                new Object[]{id},
                new AddressMapper());
    }




}
