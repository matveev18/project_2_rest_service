package com.cgi.studyproject.persistence.mapper;

import com.cgi.studyproject.persistence.entities.Person;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class PersonMapper implements RowMapper<Person> {


    @Override
    public Person mapRow(ResultSet resultSet, int i) throws SQLException {
        Person person = new Person();
        person.setIdPerson(resultSet.getLong("id_person"));
        person.setNamePerson(resultSet.getString("name"));
        person.setEmail(resultSet.getString("email"));
        person.setPassword(resultSet.getString("password"));
        person.setSalary(resultSet.getInt("salary"));

        return person;
    }
}
