package com.cgi.studyproject.persistence.entities;


public class Person {

    private Long idPerson;
    private String namePerson;
    private String email;
    private String password;
    private int salary;

    public Person() {
    }

    public Person(Long idPerson, String namePerson, String email, String password, int salary) {
        this.idPerson = idPerson;
        this.namePerson = namePerson;
        this.email = email;
        this.password = password;
        this.salary = salary;
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public String getNamePerson() {
        return namePerson;
    }

    public void setNamePerson(String namePerson) {
        this.namePerson = namePerson;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "idPerson=" + idPerson +
                ", namePerson='" + namePerson + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", salary=" + salary +
                '}';
    }
}


