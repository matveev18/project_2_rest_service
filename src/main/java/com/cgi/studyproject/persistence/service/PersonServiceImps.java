package com.cgi.studyproject.persistence.service;

import com.cgi.studyproject.persistence.entities.Person;
import com.cgi.studyproject.persistence.repository.PersonRepository;

//import com.cgi.studyproject.ws.PersonById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImps implements PersonService {

    private PersonRepository personRepository;

    @Autowired
    public PersonServiceImps(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

/*    @Override
    public PersonById getPersonById(Long id) {
        Person person = personRepository.getPersonById(id);
        return mapPersonToPersonById(person);
    }*/

    @Override
    public List<Person> getAllPersons() {
        return personRepository.getAllPersons();
    }

    @Override
    public void addPerson(Person person) {
        personRepository.addPerson(person);
    }

    @Override
    public void updatePersonById(Long id, Person person) {

    }
/*
    private PersonById mapPersonToPersonById(Person person) {
        PersonById personById = new PersonById();
        personById.setId(person.getIdPerson());
        personById.setFirstName(person.getNamePerson());
       // personById.setSurname(person.getSurname());
        personById.setEmail(person.getEmail());
     //   personById.setBirthday(person.getBirthday());
        return personById;
    }
*/

}
