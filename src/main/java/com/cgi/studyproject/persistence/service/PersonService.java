package com.cgi.studyproject.persistence.service;



import com.cgi.studyproject.persistence.entities.Person;

import java.util.List;

public interface PersonService {
  //  public PersonById getPersonById(Long id);
    public List<Person> getAllPersons();
    public void addPerson(Person person);
    public void updatePersonById(Long id, Person person);
}
