package com.cgi.studyproject;

import com.cgi.studyproject.persistence.entities.Person;
import com.cgi.studyproject.persistence.repository.PersonRepository;
import com.cgi.studyproject.persistence.service.PersonService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

@SpringBootApplication
public class StudyprojectApplication {

	public static void main(String[] args) {
//		SpringApplication.run(StudyprojectApplication.class, args);

		AnnotationConfigApplicationContext annotationConfigApplicationContext =
				new AnnotationConfigApplicationContext(StudyprojectApplication.class);



//	private PersonService personService;
		PersonService personService = annotationConfigApplicationContext.getBean(PersonService.class);
//        AddressService addressService = annotationConfigApplicationContext.getBean(AddressService.class);
		// Person person = personService.getPersonById(1L);
		List<Person> personList = personService.getAllPersons();

		System.out.println(personList);
//Person newPerson = new Person(33L, 33, LocalDate.of(1999, 12, 12), email454, firstname5, 555,  456456456 );
		//      personService.addPerson(newPerson);


	}

}
